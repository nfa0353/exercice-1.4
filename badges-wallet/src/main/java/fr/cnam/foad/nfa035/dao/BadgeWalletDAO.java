package fr.cnam.foad.nfa035.dao;

import fr.cnam.foad.nfa035.fileutils.streaming.media.ImageFileFrame;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageDeserializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageSerializerBase64StreamingImpl;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.fileutils.streaming.serializer.ImageStreamingSerializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

public class BadgeWalletDAO {
    private static Logger LOG = LogManager.getLogger(BadgeWalletDAO.class);
    private File walletDatabase;
    private ImageFileFrame media;

    /** Constructeur
     * @param s
     * @throws IOException
     */
    public BadgeWalletDAO(String s) throws IOException {
        this.walletDatabase = new File(s);
        this.media = new ImageFileFrame(walletDatabase);
    }

    /** Cette méthode serialise une image et la stocke dans l'attribut media de notre objet
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException {
        ImageStreamingSerializer serializer = new ImageSerializerBase64StreamingImpl();
        serializer.serialize(image, media);

    }

    /** Cette méthode deserialise et renvoie notre image dans le stream output
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException {
        ImageStreamingDeserializer deserializer = new ImageDeserializerBase64StreamingImpl(imageStream);
        deserializer.deserialize(media);
    }

}
